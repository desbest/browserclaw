# Browserclaw

Browserclaw is a lightweight PHP script (software) that allows you to thumbnail images and view them in a web browser, as well as displaying text and markdown files. That's it. Short and sweet.

![browserclaw screenshot](https://i.imgur.com/e7bQcZr.png)

## Did you know?

Prefix a file with an underscore to hide it, or use the `HideFiles` option in `_config.xml`

We've allowed you to place html into your page introductions by replacing angle brackets with square brackets.

To create thumbnails, where it says `index.php` in the address bar for the folder you are browsing, replace it to `thumbnailer.php`

## New features
Since desbest became the maintainer of Browserclaw (previously called Filebrowser by Lussomo), the following features have been added.

- Support for viewing text files
- Support for viewing markdown files
- Support for https:// (must be turned on in _config.xml)
- Responsive design
- The save link to save the currently viewed image or file, now works. 

## Installation Instructions

The installation of Browserclaw is pretty simple. You take all of the files contained in the zip and place them in the folder that you want to browse. Your folder must be a web-accessable directory. You do not need to copy the Browserclaw or it's files into subdirectories, subdirectories will be browsed automatically by the Browserclaw under it's default settings.

The files `_config.xml` and `_filetypes.xml` must have read permissions. If you get a permission error, chmod them 644 or contact your web host.

## License

Browserclaw has the open source GPL license.